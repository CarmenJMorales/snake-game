# Snake Game
***

![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E) ![css](https://img.shields.io/badge/CSS-239120?&style=for-the-badge&logo=css3&logoColor=white)  
  
  
Juego de la serpiente realizado en html, css y javascript  

![Gif que muestra el funcionamiento básico del juego](https://gitlab.com/CarmenJMorales/snake-game/-/blob/main/gif_prueba.gif)
